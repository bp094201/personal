package com.nrifintech.GymBookingSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymBookingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(GymBookingSystemApplication.class, args);
	}

}
